#include <stdio.h>
#include <conio.h>
#include <string.h>
#define LENGTH 33
 
int main()
{
	const char curret_password[LENGTH]="Qwerty123";
	int n=0,curcount=1,i;
	char password[LENGTH] = {0};
	printf("Enter Your Password >> ");
	while(1)
	{
		password[n] = _getch();
		if (password[n]=='\r') break;
		if (password[n]!='\b') 
		{
			if (n!=LENGTH-1)
			{
				printf("%c",'*');
				n++;
			}
		}
		else
		{
			if (n!=0)
			{
				putchar('\b');
				putchar(' ');
				putchar('\b');
				n--;
			}
		}
	}
	puts("");
	password[n]=0;
	for (i=0;i<=n;i++)
	{
		if (password[i]!=curret_password[i])
			curcount=0;
	}
	if (curcount)
		printf("Access is allowed\n");
	else
		printf("Access is denied\n");
    return 0;
}